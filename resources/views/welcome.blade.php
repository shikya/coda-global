@extends('layouts.app')

@section('content')
    @foreach($courses as $course)
    <div class="row">
      <div class="col l12">
        <div class="card-panel white">
            <span class="badge">{{ $course->type }}</span>

        <div class="row">
          <div class="black-text col l3 m3 s12">
            <div class="center">
                <img class="responsive-img" src="{{ $course->image }}" alt="">
            </div>
            <div class="row">
                <div class="left">
                    <i class="material-icons">star</i> 
                    <i class="material-icons">star</i> 
                    <i class="material-icons">star</i> 
                    <i class="material-icons">star</i> 
                    <i class="material-icons">star_border</i> 
                </div>
                <div class="right">
                    <i class="material-icons" style="float: left;">link</i>
                    <a href="{{ $course->url }}">extLink</a>
                </div>
            </div>
          </div>
          <div class="black-text col l9 m9 s12">
            <h3 class="header">{{ $course->title }}
            </h3>
                <p class="flow-text">
                    {{ $course->description }}
                </p>
          </div>
            <div class="right flow-text"><b>
            @if($course->price === '0')
                Free
            @else
                {{ $course->price }}
            @endif
            </b></div>
        </div>
        </div>
      </div>
    </div>
    @endforeach
@endsection
