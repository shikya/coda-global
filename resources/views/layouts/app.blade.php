<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Coda App</title>

    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.css"  media="screen,projection"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <style>

    header, .main, footer {
      padding-left: 250px;
    }

    @media only screen and (max-width : 992px) {
      header, .main, footer {
        padding-left: 0;
      }
    }
    .pin{
    color: black;
    border: solid;
    border-radius: 30px;
    line-height: 2;
    padding-left: 18px;
    border-width: 2px;
    border-color: blue;
    }

    .brand-logo{
      margin-left: 280px;
    }

    @media only screen and (max-width : 992px) {
      .brand-logo{
        margin-left: 0;
      }
    }
    nav .card-panel{
    padding-top: 0px;
    padding-bottom: 0px;
    }
    </style>
</head>

<body id="app-layout" style="background-color: lightgray;">

    <nav style="height: 140px;">

    <div class="nav-wrapper">
      <a href="#" class="brand-logo"><h2>LearnHub</h2><h6>a simplified courses search portal!</h6></a>
    </div>

        <ul id="slide-out" class="side-nav fixed" style="width: 250px;">

          <li>
            <img src="/img/img.jpg" alt="" class="responsive-img">
          </li>

          <li>
            <div class="row">
              <div class="col s12">
                <div class="card-panel white">
                  <span class="teal-text">
                    Categories<br>
                          <input name="group1" type="radio" id="iOS" />
                            <label for="iOS">iOS</label><br>
                          <input name="group1" type="radio" id="Andriod" />
                            <label for="Andriod">Andriod</label>
                          <input name="group1" type="radio" id="Web" />
                            <label for="Web">Web Development</label>
                          <input name="group1" type="radio" id="Software" />
                            <label for="Software">Software Engineering</label>
                  </span>
                </div>
              </div>
            </div>            
          </li>

          <li>
            <div class="row">
              <div class="col s12">
                <div class="card-panel white">
                  <span class="teal-text">
                    Sort
                      <div class="switch">
                        <label>
                          Rating
                          <input type="checkbox">
                          <span class="lever"></span>
                          Price
                        </label>
                      </div>
                    </span>
                </div>
              </div>
            </div>            
          </li>


          <li>
            <div class="row">
              <div class="col s12">
                <div class="card-panel white">
                  <span class="teal-text">
                    Total Courses : 30
                </div>
              </div>
            </div>            
          </li>



        </ul>
    </nav>

    <div class="main">

    <div class="row">
      <div class="col l12 m12 s12">
        <div class="card-panel white">
          <div class="row">
            <div class="input-field col s12">
              <i class="material-icons prefix" style="float:left;">search</i>
              <input id="icon_prefix" type="text" class="validate">
              <label for="icon_prefix">Search by Title</label>
            </div>
          </div>
        </div>
      </div>
    </div>

        @yield('content')
    </div>

    <!-- JavaScripts -->
  <script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
  <script type="text/javascript" src="js/materialize.min.js"></script>

  <script type="text/javascript">
  // Initialize collapse button
  $(".button-collapse").sideNav();
  // Initialize collapsible (uncomment the line below if you use the dropdown variation)
  //$('.collapsible').collapsible();
  </script>
</body>
</html>
