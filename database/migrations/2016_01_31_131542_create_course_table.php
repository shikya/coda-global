<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // { id, title, price, type, category, description, rating, image, url}
        // title AS text (course name)
        // price AS text (price format: Rs. ? INR / month).
        // type AS text (Beginner, Intermediate, Advanced)
        // category AS text (Technological Stack)
        // description AS text (contextual description of course)
        // rating AS float (rating of the course; on 0-5 scale)
        // image AS text (image of the course; contains URL or base64 image)
        // url AS text (url of course)

        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('price');
            $table->string('type');
            $table->string('category');
            $table->string('description');
            $table->float('rating');
            $table->text('image');
            $table->string('url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('courses');
    }
}
